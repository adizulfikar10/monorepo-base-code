# How to use

After clone please do this

```
yarn install
yarn prepare
```

To install library in specify app (do not cd in apps/app-name)

```
yarn workspace emtrade-dashboard add moment
```

and then to start dev mode (with hot reload)

```
yarn dev
```

to start dev mode an specify app

```
yarn workspace emtrade-dashboard dev
```

to build

```
yarn build
```

## What's inside?

This monorepo uses [Yarn](https://classic.yarnpkg.com/lang/en/) as a package manager. It includes the following packages/apps:

### Apps and Packages

- `docs`: a [Next.js](https://nextjs.org) app
- `web`: another [Next.js](https://nextjs.org) app
- `ui`: a stub React component library shared by both `web` and `docs` applications
- `config`: `eslint` configurations (includes `eslint-config-next` and `eslint-config-prettier`)
- `tsconfig`: `tsconfig.json`s used throughout the monorepo

Each package/app is 100% [TypeScript](https://www.typescriptlang.org/).
