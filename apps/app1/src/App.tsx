import React from 'react'
import CounterAppOne from './components/CounterAppOne'

const App = () => (
  <div>
    <div>APP 1</div>
    <div>
      <CounterAppOne />
    </div>
  </div>
)

export default App
