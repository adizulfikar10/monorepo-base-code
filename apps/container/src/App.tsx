import React from 'react'

const CounterAppOne = React.lazy(() => import('app1/CounterAppOne'))

const App = () => (
  <div>
    <div color="#fff">Container</div>
    <React.Suspense fallback={<div>loading</div>}>
      <div>
        <div>APP-1</div>
        <CounterAppOne />
      </div>
    </React.Suspense>
  </div>
)

export default App
